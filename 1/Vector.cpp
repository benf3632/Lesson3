#include "Vector.h"

Vector::Vector(int n) : _capacity(n), _size(0), _resizeFactor(n)
{
	if (n < MIN_VECTOR_CAPACITY)
	{
		this->_elements = new int[MIN_VECTOR_CAPACITY];
	}
	else
	{
		this->_elements = new int[n];
	}
}

Vector::~Vector()
{
	if (this->_elements != nullptr)
	{
		delete[] this->_elements;
		this->_elements = nullptr;
	}
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return !this->_size;
}


void Vector::push_back(const int& val)
{
	if (_size == _capacity)
	{
		reserve(_capacity + _resizeFactor);
	}

	this->_elements[this->_size] = val;
	this->_size++;
}

int Vector::pop_back()
{
	int& size = this->_size;
	if (size)
	{
		size--;
		return this->_elements[size + 1];
	}
	else
	{
		std::cerr << "Error: pop from empty vector" << std::endl;
		return -9999;
	}
}

void Vector::reserve(int n)
{
	int resize = 0;
	int* temp = nullptr;

	if (n >= _capacity)
	{
		if (n % _resizeFactor > 0) //checks if the reserve size is not devidable by the resize factor
		{
			resize = n / _resizeFactor + 1; //adds one beacuse div with ints takes the number in the left of the decimal point and its not enough for the reserve
		}
		else
		{
			resize = n / _resizeFactor; //exact as the reserve size
		}

		temp = new int[resize * _resizeFactor]; //creates temp arr

		for (int i = 0; i < _size; i++)
		{
			temp[i] = _elements[i];
		}

		delete[] this->_elements;
		this->_elements = temp;
		this->_capacity = resize * this->_resizeFactor;
	}
}

void Vector::resize(int n)
{
	if (n > this->_capacity)
	{
		reserve(n + _capacity);
	}

	this->_size = n;
}

void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int & val)
{
	int size = this->_size;
	resize(n);
	if (n > size)
	{
		for (int i = size; i < this->_size; i++)
		{
			this->_elements[i] = val;
		}
	}
}


Vector::Vector()
{
	Vector(2);
}

Vector::Vector(const Vector & other)
{
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_elements = new int[other._capacity];

	for (int i = 0; i < other._size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
}

Vector& Vector::operator=(const Vector & other)
{

	this->~Vector();
	this->Vector::Vector(other);
	return *this;
}

int & Vector::operator[](int n) const
{
	if (n >= _size)
	{
		std::cerr << "The element is not in the Range!!!" << endl;
		return _elements[0];
	}

	return _elements[n];
}


Vector Vector::operator+(const Vector & a)
{
	int size = 0;
	Vector temp;
	if (_size <= a._size)
	{
		size = _size;
		temp.~Vector();
		temp.Vector::Vector(*this);
	}
	else
	{
		size = a._size;
		temp.~Vector();
		temp.Vector::Vector(a);
	}

	for (int i = 0; i < size; i++)
	{
		temp._elements[i] = _elements[i] + a._elements[i];
	}

	return temp;
}


void Vector::operator+=(const Vector & other)
{
	int size = 0;

	if (_size <= other._size)
	{
		size = _size;
	}
	else
	{
		size = other._size;
	}

	for (int i = 0; i < size; i++)
	{
		this->_elements[i] += other._elements[i];
	}
}

Vector Vector::operator-(const Vector & other)
{
	Vector temp;
	int size = 0;
	if (_size > other._size)
	{
		size = other._size;
		temp.~Vector();
		temp.Vector::Vector(other);
	}
	else
	{
		size = _size;
		temp.~Vector();
		temp.Vector::Vector(other);
	}

	for (int i = 0; i < size; i++)
	{
		temp._elements[i] = _elements[i] - other._elements[i];
	}

	return temp;
}

void Vector::operator-=(const Vector & other)
{
	int size = 0;
	if (_size > other._size)
	{
		size = other._size;
	}
	else
	{
		size = _size;
	}
	for (int i = 0; i < size; i++)
	{
		this->_elements[i] -= other._elements[i];
	}
}


ostream & operator<<(ostream & os, const Vector & other)
{
	os << "VectorInfo:\n" << "Capacity is " << other._capacity << endl;
	os << "Size is " << other._size << endl;
	os << "Data is ";

	for (int i = 0; i < other._size; i++)
	{
		if (i + 1 == other._size)
		{
			os << other._elements[i] << endl;
		}
		else
		{
			os << other._elements[i] << ", ";
		}
	}
	os << endl;

	return os;
}